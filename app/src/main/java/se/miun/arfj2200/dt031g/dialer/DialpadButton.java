package se.miun.arfj2200.dt031g.dialer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

public class DialpadButton extends ConstraintLayout {

    private TextView title;
    private TextView message;

    public DialpadButton(@NonNull Context context) {
        super(context);
        this.init(null);
    }

    public DialpadButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.sample_dialpad_button, this);

        @SuppressLint("Recycle") TypedArray customAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.DialpadButton);

        this.title = findViewById(R.id.title);
        this.message = findViewById(R.id.message);

        this.setTitle(customAttributes.getString(R.styleable.DialpadButton_title));
        this.setMessage(customAttributes.getString(R.styleable.DialpadButton_message));
        this.setOnClickListener(view -> onClick());
    }

    private void onClick() {
        SoundPlayer.getInstance(getContext()).playSound(this);
        animateClick();
    }

    private void animateClick() {
        Animation fadeOut = new AlphaAnimation(1f, 0.6f);
        fadeOut.setDuration(300);
        this.startAnimation(fadeOut);
        this.postDelayed(() -> {}, 300);

        Animation fadeIn = new AlphaAnimation(0.6f, 1f);
        fadeIn.setDuration(300);
        this.startAnimation(fadeIn);
        this.postDelayed(() -> {}, 300);
    }

    public void setTitle(String title) {
        if (title == null) return;
        this.title.setText(title.substring(0, 1));
    }

    public String getTitle() {
        return this.title.getText().toString();
    }

    public void setMessage(String message) {
        if (message == null) return;
        this.message.setText(message.substring(0, Math.min(3, message.length())));
    }
}