package se.miun.arfj2200.dt031g.dialer;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class DialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SoundPlayer.getInstance(getApplicationContext());
        setContentView(R.layout.activity_dial);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SoundPlayer.getInstance(getApplicationContext()).destroy();
    }
}