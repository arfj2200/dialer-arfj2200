package se.miun.arfj2200.dt031g.dialer;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import java.util.LinkedHashMap;

public class SoundPlayer {

    private final SoundPool soundPool;
    private final LinkedHashMap<String, Integer> sounds;
    private static SoundPlayer soundPlayer;

    public static SoundPlayer getInstance(Context context) {
        if (soundPlayer == null) soundPlayer = new SoundPlayer(context);
        return soundPlayer;
    }

    private SoundPlayer(Context context) {
        this.soundPool = new SoundPool.Builder()
                .setMaxStreams(12)
                .setAudioAttributes(new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_NAVIGATION_GUIDANCE)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()).build();

        this.sounds = new LinkedHashMap<String, Integer>();
        this.sounds.put(context.getResources().getString(R.string.zero), soundPool.load(context, R.raw.zero, 1));
        this.sounds.put(context.getResources().getString(R.string.one), soundPool.load(context, R.raw.one, 1));
        this.sounds.put(context.getResources().getString(R.string.two), soundPool.load(context, R.raw.two, 1));
        this.sounds.put(context.getResources().getString(R.string.three), soundPool.load(context, R.raw.three, 1));
        this.sounds.put(context.getResources().getString(R.string.four), soundPool.load(context, R.raw.four, 1));
        this.sounds.put(context.getResources().getString(R.string.five), soundPool.load(context, R.raw.five, 1));
        this.sounds.put(context.getResources().getString(R.string.six), soundPool.load(context, R.raw.six, 1));
        this.sounds.put(context.getResources().getString(R.string.seven), soundPool.load(context, R.raw.seven, 1));
        this.sounds.put(context.getResources().getString(R.string.eight), soundPool.load(context, R.raw.eight, 1));
        this.sounds.put(context.getResources().getString(R.string.nine),soundPool.load(context, R.raw.nine, 1));
        this.sounds.put(context.getResources().getString(R.string.star), soundPool.load(context, R.raw.star, 1));
        this.sounds.put(context.getResources().getString(R.string.pound), soundPool.load(context, R.raw.pound, 1));
    }

    public void playSound(DialpadButton dialpadButton) {
        Integer sound = this.sounds.get(dialpadButton.getTitle());
        if (sound == null) return;
        this.soundPool.play(sound, 1, 1, 1, 0, 1);
    }

    public void destroy() {
        this.soundPool.release();
        soundPlayer = null;
    }
}