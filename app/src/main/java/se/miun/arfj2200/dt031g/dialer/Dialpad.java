package se.miun.arfj2200.dt031g.dialer;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

public class Dialpad extends ConstraintLayout {

    public Dialpad(@NonNull Context context) {
        super(context);
        this.init(null);
    }

    public Dialpad(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.dialpad, this);
    }
}