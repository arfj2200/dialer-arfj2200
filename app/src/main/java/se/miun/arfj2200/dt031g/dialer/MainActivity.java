package se.miun.arfj2200.dt031g.dialer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String state = "aboutState";
    private boolean aboutState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onDial(View view) {
        startActivity(new Intent(this, DialActivity.class));
    }

    public void onCallList(View view) {
        startActivity(new Intent(this, CallListActivity.class));
    }

    public void onSettings(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    public void onMaps(View view) {
        startActivity(new Intent(this, MapsActivity.class));
    }

    public void onAbout(View view) {
        if (aboutState) {
            aboutToast();
        } else {
            aboutDialog();
        }
    }

    public void aboutDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.alertdialog_about_title)
                .setMessage(R.string.alertdialog_about_message)
                .setPositiveButton(R.string.alertdialog_about_button, null)
                .show();
        this.aboutState = true;
    }

    public void aboutToast() {
        Toast.makeText(getApplicationContext(), R.string.about_toast, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(state, this.aboutState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.aboutState = savedInstanceState.getBoolean(state);
    }
}